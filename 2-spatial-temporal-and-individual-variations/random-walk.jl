using Plots

"""
Coordinate of the `Walker`.
"""
mutable struct Walker{D}
    coord::NTuple{D,Integer}
end

"""
    Walker(kwargs...)

Sugar to build `Walker`s.

Before to initialize a 1D `Walker` starting at `0`,
one had to do: `Walker((0,))` has the initial coordinates had to be given under the form
of a tuple.
But now, thanks to this sweet function, one can do: `Walker(0)`.
Obviously, this applies for any dimension.
For instance, at 3d: `Walker((0, 0, 0))` can be replaced with `Walker(0, 0, 0)`.
"""
function Walker(kwargs...)
    D = length(keys(kwargs))
    Walker{D}(ntuple(i -> kwargs[i], D))
end

"""
Dimension of the walk.
"""
dim(w::Walker) = length(w.coord)

"""
The `w`alker moves from one step.
"""
function step!(w::Walker)
    D = dim(w)
    direction = rand(1:D)
    w.coord = ntuple(i -> i == direction ? w.coord[i] + rand([-1, 1]) : w.coord[i], D)
end

"""
The `w`alker walks for `n` steps.
"""
function walk(w::Walker, n::Integer)
    trajectory = zeros(Integer, n + 1, dim(w))
    trajectory[1, :] = vcat(w.coord...)
    for i in 1:n
        step!(w)
        trajectory[i+1, :] = vcat(w.coord...)
    end
    trajectory
end

dist(coord) = sqrt(sum(coord .^ 2))
dist(traj::AbstractMatrix) = [dist(traj[i, :]) for i in 1:size(traj, 1)]

# Visualize 1D walks.
# Plot the position of the Walker vs. the number of steps.
n_walker = 3
n = 1_000
plot()
for i in 1:n_walker
    w = Walker(0)
    traj = walk(w, n)
    scatter!(0:n, traj; label = "Walker $i", markersize = 4)
end
xlabel!("Step")
ylabel!("Position")

# Visualize 2D walks.
# Plot the Walker trajectories, y vs. x coordinates.
n_walker = 3
n = 10_000
plot()
for i in 1:n_walker
    w = Walker(0, 0)
    traj = walk(w, n)
    x, y = traj[:, 1], traj[:, 2]
    scatter!(x, y; label = "Walker $i", markersize = 4, alpha = 0.6)
end
xlabel!("Position x")
ylabel!("Position y")
